<?php

namespace App\Entity;

use App\Repository\RecetteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RecetteRepository::class)]
class Recette
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $titre = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\Column]
    private ?bool $recetteBasique = null;

    #[ORM\Column(nullable: true)]
    private ?int $tempsDeRepos = null;

    #[ORM\Column(nullable: true)]
    private ?int $tempsDePreparation = null;

    #[ORM\Column(nullable: true)]
    private ?int $tempsDeCuisson = null;

    #[ORM\Column(type: Types::ARRAY)]
    private array $ingredients = [];

    #[ORM\Column(type: Types::ARRAY)]
    private array $etapes = [];

    #[ORM\Column(length: 255)]
    private ?string $fileImage = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\ManyToMany(targetEntity: Allergene::class, mappedBy: 'recette')]
    private Collection $allergenes;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'recettes')]
    private Collection $user;

    #[ORM\OneToMany(mappedBy: 'recette', targetEntity: View::class)]
    private Collection $view;

    #[ORM\ManyToOne(inversedBy: 'recettes')]
    private ?Regime $regime = null;

    public function __construct()
    {
        $this->allergenes = new ArrayCollection();
        $this->user = new ArrayCollection();
        $this->view = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function isRecetteBasique(): ?bool
    {
        return $this->recetteBasique;
    }

    public function setRecetteBasique(bool $recetteBasique): self
    {
        $this->recetteBasique = $recetteBasique;

        return $this;
    }

    public function getTempsDeRepos(): ?int
    {
        return $this->tempsDeRepos;
    }

    public function setTempsDeRepos(?int $tempsDeRepos): self
    {
        $this->tempsDeRepos = $tempsDeRepos;

        return $this;
    }

    public function getTempsDePreparation(): ?int
    {
        return $this->tempsDePreparation;
    }

    public function setTempsDePreparation(?int $tempsDePreparation): self
    {
        $this->tempsDePreparation = $tempsDePreparation;

        return $this;
    }

    public function getTempsDeCuisson(): ?int
    {
        return $this->tempsDeCuisson;
    }

    public function setTempsDeCuisson(?int $tempsDeCuisson): self
    {
        $this->tempsDeCuisson = $tempsDeCuisson;

        return $this;
    }

    public function getIngredients(): array
    {
        return $this->ingredients;
    }

    public function setIngredients(array $ingredients): self
    {
        $this->ingredients = $ingredients;

        return $this;
    }

    public function getEtapes(): array
    {
        return $this->etapes;
    }

    public function setEtapes(array $etapes): self
    {
        $this->etapes = $etapes;

        return $this;
    }

    public function getFileImage(): ?string
    {
        return $this->fileImage;
    }

    public function setFileImage(string $fileImage): self
    {
        $this->fileImage = $fileImage;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection<int, Allergene>
     */
    public function getAllergenes(): Collection
    {
        return $this->allergenes;
    }

    public function addAllergene(Allergene $allergene): self
    {
        if (!$this->allergenes->contains($allergene)) {
            $this->allergenes->add($allergene);
            $allergene->addRecette($this);
        }

        return $this;
    }

    public function removeAllergene(Allergene $allergene): self
    {
        if ($this->allergenes->removeElement($allergene)) {
            $allergene->removeRecette($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(User $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user->add($user);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->user->removeElement($user);

        return $this;
    }

    /**
     * @return Collection<int, View>
     */
    public function getView(): Collection
    {
        return $this->view;
    }

    public function addView(View $view): self
    {
        if (!$this->view->contains($view)) {
            $this->view->add($view);
            $view->setRecette($this);
        }

        return $this;
    }

    public function removeView(View $view): self
    {
        if ($this->view->removeElement($view)) {
            // set the owning side to null (unless already changed)
            if ($view->getRecette() === $this) {
                $view->setRecette(null);
            }
        }

        return $this;
    }

    public function getRegime(): ?Regime
    {
        return $this->regime;
    }

    public function setRegime(?Regime $regime): self
    {
        $this->regime = $regime;

        return $this;
    }
}
