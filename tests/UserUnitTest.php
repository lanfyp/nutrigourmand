<?php

namespace App\Tests;

use DateTime;
use App\Entity\User;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new User();
        $dateTime = new DateTimeImmutable();

        $user->setEmail('true@test.com')
            ->setPassword('password')
            ->setRoles(['ROLE_USER'])
            ->setNom('Nom')
            ->setPrenom('Prenom')
            ->setCreatedAt($dateTime);

        $this->assertTrue($user->getEmail() === 'true@test.com');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getRoles() === ['ROLE_USER']);
        $this->assertTrue($user->getNom() === 'Nom');
        $this->assertTrue($user->getPrenom() === 'Prenom');
        $this->assertTrue($user->getCreatedAt() === $dateTime);
    }

    public function testIsFalse(): void
    {
        $user = new User();
        $dateTime = new DateTimeImmutable();

        $user->setEmail('true@test.com')
            ->setPassword('password')
            ->setNom('Nom')
            ->setPrenom('Prenom')
            ->setCreatedAt($dateTime);

        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getNom() === 'false');
        $this->assertFalse($user->getPrenom() === 'false');
        $this->assertFalse($user->getCreatedAt() === 'false');
    }

    public function testIsEmpty(): void
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getNom());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getCreatedAt());
    }
}
