<?php

namespace App\Tests;

use App\Entity\Regime;
use PHPUnit\Framework\TestCase;

class RegimeUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $regime = new Regime();

        $regime->setNom('Nom')
        ->setDescription('description')
        ->setSlug('slug');

        $this->assertTrue($regime->getNom() === 'Nom');
        $this->assertTrue($regime->getDescription() === 'description');
        $this->assertTrue($regime->getSlug() === 'slug');
    }

    public function testIsFalse(): void
    {
        $regime = new Regime();

        $regime->setNom('Nom')
        ->setDescription('description')
        ->setSlug('slug');

        $this->assertFalse($regime->getNom() === 'false');
        $this->assertFalse($regime->getDescription() === 'false');
        $this->assertFalse($regime->getSlug() === 'false');
    }

    public function testIsEmpty(): void
    {
        $regime = new Regime();

        $this->assertEmpty($regime->getNom());
        $this->assertEmpty($regime->getDescription());
        $this->assertEmpty($regime->getSlug());
    }
}
