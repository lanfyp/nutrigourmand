<?php

namespace App\Tests;

use App\Entity\View;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class ViewUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $view = new View();
        $dateTime = new DateTimeImmutable();

        $view->setTitre('Titre')
        ->setDescription('description')
        ->setNote(5)
        ->setCreatedAt($dateTime);

        $this->assertTrue($view->getTitre() === 'Titre');
        $this->assertTrue($view->getDescription() === 'description');
        $this->assertTrue($view->getNote() === 5);
        $this->assertTrue($view->getCreatedAt() === $dateTime);
    }

    public function testIsFalse(): void
    {
        $view = new View();
        $dateTime = new DateTimeImmutable();

        $view->setTitre('Titre')
        ->setDescription('description')
        ->setNote(5)
        ->setCreatedAt($dateTime);

        $this->assertFalse($view->getTitre() === 'false');
        $this->assertFalse($view->getDescription() === 'false');
        $this->assertFalse($view->getNote() === 1);
        $this->assertFalse($view->getCreatedAt() === 'false');
    }

    public function testIsEmpty(): void
    {
        $view = new View();

        $this->assertEmpty($view->getTitre());
        $this->assertEmpty($view->getDescription());
        $this->assertEmpty($view->getNote());
        $this->assertEmpty($view->getCreatedAt());
    }
}
