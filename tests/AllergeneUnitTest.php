<?php

namespace App\Tests;

use App\Entity\Allergene;
use PHPUnit\Framework\TestCase;

class AllergeneUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $allergene = new Allergene();

        $allergene->setNom('Nom')
        ->setDescription('description')
        ->setSlug('slug');

        $this->assertTrue($allergene->getNom() === 'Nom');
        $this->assertTrue($allergene->getDescription() === 'description');
        $this->assertTrue($allergene->getSlug() === 'slug');
    }

    public function testIsFalse(): void
    {
        $allergene = new Allergene();

        $allergene->setNom('Nom')
        ->setDescription('description')
        ->setSlug('slug');

        $this->assertFalse($allergene->getNom() === 'false');
        $this->assertFalse($allergene->getDescription() === 'false');
        $this->assertFalse($allergene->getSlug() === 'false');
    }

    public function testIsEmpty(): void
    {
        $allergene = new Allergene();

        $this->assertEmpty($allergene->getNom());
        $this->assertEmpty($allergene->getDescription());
        $this->assertEmpty($allergene->getSlug());
    }
}
