<?php

namespace App\Tests;

use DateTimeImmutable;
use App\Entity\Recette;
use PHPUnit\Framework\TestCase;

class RecetteUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $recette = new Recette();
        $dateTime = new DateTimeImmutable();

        $recette->setTitre('Titre')
            ->setDescription('description')
            ->setRecetteBasique(true)
            ->setTempsDeRepos(5)
            ->setTempsDeCuisson(5)
            ->setTempsDePreparation(5)
            ->setIngredients(['ingredients'])
            ->setEtapes(['etapes'])
            ->setFileImage('fileImage')
            ->setSlug('slug')
            ->setCreatedAt($dateTime);

        $this->assertTrue($recette->getTitre() === 'Titre');
        $this->assertTrue($recette->getDescription() === 'description');
        $this->assertTrue($recette->isRecetteBasique() === true);
        $this->assertTrue($recette->getTempsDeRepos() === 5);
        $this->assertTrue($recette->getTempsDeCuisson() === 5);
        $this->assertTrue($recette->getTempsDePreparation() === 5);
        $this->assertTrue($recette->getIngredients() === ['ingredients']);
        $this->assertTrue($recette->getEtapes() === ['etapes']);
        $this->assertTrue($recette->getFileImage() === 'fileImage');
        $this->assertTrue($recette->getSlug() === 'slug');
        $this->assertTrue($recette->getCreatedAt() === $dateTime);
    }

    public function testIsFalse(): void
    {
        $recette = new Recette();
        $dateTime = new DateTimeImmutable();

        $recette->setTitre('Titre')
            ->setDescription('description')
            ->setRecetteBasique(true)
            ->setTempsDeRepos(5)
            ->setTempsDeCuisson(5)
            ->setTempsDePreparation(5)
            ->setIngredients(['ingredients'])
            ->setEtapes(['etapes'])
            ->setFileImage('fileImage')
            ->setSlug('slug')
            ->setCreatedAt($dateTime);

        $this->assertFalse($recette->getTitre() === 'false');
        $this->assertFalse($recette->getDescription() === 'false');
        $this->assertFalse($recette->isRecetteBasique() === false);
        $this->assertFalse($recette->getTempsDeRepos() === 1);
        $this->assertFalse($recette->getTempsDeCuisson() === 1);
        $this->assertFalse($recette->getTempsDePreparation() === 1);
        $this->assertFalse($recette->getIngredients() === ['false']);
        $this->assertFalse($recette->getEtapes() === ['false']);
        $this->assertFalse($recette->getFileImage() === 'false');
        $this->assertFalse($recette->getSlug() === 'false');
        $this->assertFalse($recette->getCreatedAt() === 'false');
    }

    public function testIsEmpty(): void
    {
        $recette = new Recette();

        $this->assertEmpty($recette->getTitre());
        $this->assertEmpty($recette->getDescription());
        $this->assertEmpty($recette->isRecetteBasique());
        $this->assertEmpty($recette->getTempsDeRepos());
        $this->assertEmpty($recette->getTempsDeCuisson());
        $this->assertEmpty($recette->getTempsDePreparation());
        $this->assertEmpty($recette->getIngredients());
        $this->assertEmpty($recette->getEtapes());
        $this->assertEmpty($recette->getFileImage());
        $this->assertEmpty($recette->getSlug());
        $this->assertEmpty($recette->getCreatedAt());
    }
}
