# NutriGourmand

NutriGourmand est le site de Sandrine Coupart. En tant que professionnelle de santé, elle prend en charge des patients dans le cadre de consultations diététiques. Diététicienne-nutritionniste dont le cabinet est situé à Caen, mme Coupart anime également des ateliers de prévention et d’information sur la nutrition. NutriGourmand permet de proposer des recettes adaptées à chaque régime en fonction des allergènes de chacun.

## Environment de développement

### Prérequis

* PHP 8.1
* Composer
* Symfony CLI
* Docker
* Docker Compose
* node js et npm

Vous pouvez vérifier les pré requis (sauf Docker et Docker-compose) avec la commande suivante (de la CLI Symfony):

```bash
symfony check:requirements
```

### Lancer l'environnement de développement

```bash
composer install
npm install
npm run build
docker-compose up -d
symfony serve -d
```

### Arrêter l'environnement de développement

```bash
docker-compose down
symfony server:stop
```

## Git

### Créer une nouvelle branche

```bash
git checkout -b "nom_de_la_branche"
git push -u origin "nom_de_la_branche"
```

### Merge Request

Distant:
Choisr la branche "main" comme branche cible et la branche de développement comme branche source.
Ajouter un titre et une description à la Merge Request.
Ajouter un Assignee.
Puis cliquer sur "Create Merge Request".
Puis approuver la Merge Request.
Et la merger.
Local:
```bash
git checkout main
git pull
git branch -d "nom_de_la_branche"
git branch
```
Sur trello ajouter la merge avec le power-up "GitLab Merge Request".

## Lancer des tests

```bash
php bin/phpunit --testdox
```

