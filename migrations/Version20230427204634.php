<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230427204634 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE allergene_user (allergene_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_9F6A80C74646AB2 (allergene_id), INDEX IDX_9F6A80C7A76ED395 (user_id), PRIMARY KEY(allergene_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE allergene_recette (allergene_id INT NOT NULL, recette_id INT NOT NULL, INDEX IDX_8A90E5624646AB2 (allergene_id), INDEX IDX_8A90E56289312FE9 (recette_id), PRIMARY KEY(allergene_id, recette_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE recette_user (recette_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_C0933C1289312FE9 (recette_id), INDEX IDX_C0933C12A76ED395 (user_id), PRIMARY KEY(recette_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE allergene_user ADD CONSTRAINT FK_9F6A80C74646AB2 FOREIGN KEY (allergene_id) REFERENCES allergene (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE allergene_user ADD CONSTRAINT FK_9F6A80C7A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE allergene_recette ADD CONSTRAINT FK_8A90E5624646AB2 FOREIGN KEY (allergene_id) REFERENCES allergene (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE allergene_recette ADD CONSTRAINT FK_8A90E56289312FE9 FOREIGN KEY (recette_id) REFERENCES recette (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE recette_user ADD CONSTRAINT FK_C0933C1289312FE9 FOREIGN KEY (recette_id) REFERENCES recette (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE recette_user ADD CONSTRAINT FK_C0933C12A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE allergene CHANGE decription description LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE recette ADD regime_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE recette ADD CONSTRAINT FK_49BB639035E7D534 FOREIGN KEY (regime_id) REFERENCES regime (id)');
        $this->addSql('CREATE INDEX IDX_49BB639035E7D534 ON recette (regime_id)');
        $this->addSql('ALTER TABLE user ADD regime_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64935E7D534 FOREIGN KEY (regime_id) REFERENCES regime (id)');
        $this->addSql('CREATE INDEX IDX_8D93D64935E7D534 ON user (regime_id)');
        $this->addSql('ALTER TABLE view ADD recette_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE view ADD CONSTRAINT FK_FEFDAB8E89312FE9 FOREIGN KEY (recette_id) REFERENCES recette (id)');
        $this->addSql('CREATE INDEX IDX_FEFDAB8E89312FE9 ON view (recette_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE allergene_user DROP FOREIGN KEY FK_9F6A80C74646AB2');
        $this->addSql('ALTER TABLE allergene_user DROP FOREIGN KEY FK_9F6A80C7A76ED395');
        $this->addSql('ALTER TABLE allergene_recette DROP FOREIGN KEY FK_8A90E5624646AB2');
        $this->addSql('ALTER TABLE allergene_recette DROP FOREIGN KEY FK_8A90E56289312FE9');
        $this->addSql('ALTER TABLE recette_user DROP FOREIGN KEY FK_C0933C1289312FE9');
        $this->addSql('ALTER TABLE recette_user DROP FOREIGN KEY FK_C0933C12A76ED395');
        $this->addSql('DROP TABLE allergene_user');
        $this->addSql('DROP TABLE allergene_recette');
        $this->addSql('DROP TABLE recette_user');
        $this->addSql('ALTER TABLE allergene CHANGE description decription LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE view DROP FOREIGN KEY FK_FEFDAB8E89312FE9');
        $this->addSql('DROP INDEX IDX_FEFDAB8E89312FE9 ON view');
        $this->addSql('ALTER TABLE view DROP recette_id');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64935E7D534');
        $this->addSql('DROP INDEX IDX_8D93D64935E7D534 ON user');
        $this->addSql('ALTER TABLE user DROP regime_id');
        $this->addSql('ALTER TABLE recette DROP FOREIGN KEY FK_49BB639035E7D534');
        $this->addSql('DROP INDEX IDX_49BB639035E7D534 ON recette');
        $this->addSql('ALTER TABLE recette DROP regime_id');
    }
}
